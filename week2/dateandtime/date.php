<?php
//date formats
	echo "today is " . date("Y/m/d") . "<br>";
	echo "today is " . date("Y.m.d") . "<br>";
	echo "today is " . date("Y-m-d") . "<br>";
	echo "today is " . date("l"). "<br>";

?>

	© 2010-<?php echo date("Y")."<br>";?>

<?php
	echo "the time is " . date("H:i:sa")."<br>";
?>

<?php
//create a date with mktime
	$d=mktime(12, 60, 60, 3, 26, 2020);
	echo "created date is " . date("Y-m-d h:i:sa", $d)."<br>";
?>

<?php
//strtotime is convert date into human readable date string into a Unix timestamp
	$d=strtotime("tomorrow");
	echo date("Y-m-d h:i:sa", $d) . "<br>";

	$d=strtotime("next Saturday");
	echo date("Y-m-d h:i:sa", $d) . "<br>";

	$d=strtotime("+3 Months");
	echo date("Y-m-d h:i:sa", $d) . "<br>";
?>

<?php
	$d1=strtotime("march 30");
	$d2=ceil(($d1-time())/60/60/24);
	echo "There are " . $d2 ." days until 30th of march.";
?>