<?php
echo strlen("good morning")."<br>";
echo strrev("good morning")."<br>";
echo str_replace("morning", "afternoon", "good morning")."<br>";
echo str_replace("morning", "night", "good morning")."<br>";//case sensitive
echo str_ireplace("WORLD","Peter","Hello world!")."<br>";//case insensitive
echo str_word_count("good morning")."<br>";
echo strpos("good morning","i")."<br>";//find position of first occurance case sensitive

echo stripos("good morning","I")."<br>";//find position of first occurance case insensitive


echo strrpos("good morning", "o");//position of last occurance case sensitive
echo "<br>";
echo "<br>";

$str =addcslashes("hello", "l")."<br>";
echo $str;
$str =addslashes('how "are" you?')."<br>";
echo "$str";
$str=bin2hex("hello world")."<br>";
echo $str;
$str="good morning";
echo chop($str,"morning")."<br>";//remove string from right side
echo chr(53)."<br>";//return character from ascii
$str="good morning";
echo chunk_split($str,1,'*')."<br>";


$str = "Hello world!";
echo convert_uuencode($str)."<br>";//convert into encode

$str = crc32("Hello World!");
printf("%u\n",$str);//return crc32


echo "<pre>";
$str="good morning"."<br>";
print_r(explode(" ", $str));//convert into array
print_r( str_split("hello"));
echo "</pre>";

$arr = array('Hello','World!','Beautiful','Day!');
echo implode(" ",$arr);//convert into string

echo ucwords("hello world")."<br>";//first later upper case

echo ucfirst("good morning")."<br><br>";

$str = "good morning!";
echo $str . "<br>";
echo trim($str,"grg")."<br>";


$str = "Hello World!";
echo $str . "<br>";
echo ltrim($str,"Hello")."<br>";

echo substr_replace("Hello","world",1)."<br>";//hello replace with world
echo substr_count("hello  sdfg sdf x asd asd asd  world world", "asd")."<br>";

echo substr_compare("hello world", "hello world", 0)."<br>";

echo strtolower("HELLO")."<br>";
echo strtoupper('hello')."<br>";
echo substr("Hello world",6)."<br>";
echo strtr("hello  world", "o","0")."<br>";

$str="good morning good morning good morning good morning"."<br>";
$token = strtok($str, " ");

while ($token !== false)
{
echo "$token<br>";
$token = strtok(" ");
}


echo strspn("good jfkndcm, cnbcvvbxcnjhjgdhkjlcjdhgjnvgchjn ", "g")."<br>";

echo strrchr("Hello world! What a beautiful day!","world")."<br>";

echo str_pad("good morning",20,"*" )."<br>";

echo strip_tags("Hello <i><b>world!</b></i>","<b><i>")."<br>";

echo str_shuffle("good morning")."<br>";

echo str_repeat("good morning", 10)."<br>";

echo similar_text("hello world", "ho");
?>