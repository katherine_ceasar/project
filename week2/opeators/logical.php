<?php
$a=50;
$b=100;
//and
if($a==50 and $b==100)
{
	echo "both a and b are true <br>";
}

//or
if($a==10 or $b==100)
{
	echo "one is true <br>";
}

//xor
if($a==10 xor $b==100)
{
	echo "true either x or y not both <br>";
}
//&& both value are true

if($a==50 && $b==100)
{
	echo "good morning <br>";
}
// || true if either one value is true
if($a==50 || $b==80)
{
	echo "good morning <br>";
}

//! not equal
if($a !== 60)
{
	echo "a is not equal ";
}
?>