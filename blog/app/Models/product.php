<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    use HasFactory;
    protected $fillable = ['*'];
   // protected $fillable = ['id', 'category_id', 'name','image','product_code','price','sale_price','quantity'];
    protected $table = "products";

     public function product()
    {
        return $this->hasMany(Product::class,'category_id');
    }
   
}

