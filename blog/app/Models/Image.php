<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'product_id', 'image','status'];
    protected $table = 'Images';
   
    public function Image()
    {
       return $this->hasOne('Image');
    }
     public $timestamps = false;
}
