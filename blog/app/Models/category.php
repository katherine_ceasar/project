<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    use HasFactory;
    protected $fillable = ['id','name','image','no_of_order','order_id','status'];
    public function categories()
    {
    return $this->belongsTo(Category::class,'category_id');
    }
   
}
