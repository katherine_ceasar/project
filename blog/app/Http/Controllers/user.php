<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\signup;
use Illuminate\Support\Facades\Hash;
class user extends Controller
{
   function login()
   {
       return view('login');
   }
    
   function signup()
   {
       return view('signup');
   }
  function addData(Request $req)
    {
        $req->validate([
            'user_name' => 'required',
            'birthdate' => 'required',
            'email' => 'required|email|unique:signups',
            'password' => 'required|min:5|max:12'
            ]);
        
        $signup = new signup;
        $signup->user_name = $req->user_name;
        $signup->birthdate = $req->birthdate;
        $signup->email=$req->email;
        $signup-> password = Hash::make($req['password']);
        //$signup->password=$req->password;
        $signup->save();
        return redirect('login')->with('success','signup successfully!');
      
    }
 
   function check(Request $req)
   {
    $req->validate([
        
        'email' => 'required',
        'password' => 'required'
        ]);

        $signup = signup::where('email','=', $req->email)->first();
        if($signup)
        {
            if(Hash::check($req->password, $signup->password))
            {
                $req->session()->put('LoggedUser',$signup->id);
                return redirect('index');

            }
            else
            {
                return back()->with('error','invalid password');
            }

        }
        else
        {
            return back()->with('error','no accound found for this email');
        }
   }

   function index()
   {
       if(session()->has('LoggedUser'))
       {
           $signup = signup::where('id','=',session('LoggedUser'))->first();
       }
    return view('index');
   }
    
   function logout()
   {
    if(session()->has('LoggedUser'))
    {
        session()->pull('LoggedUser');
        return redirect('login');
    }
   }
}
