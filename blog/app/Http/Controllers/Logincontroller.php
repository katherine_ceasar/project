<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\MockObject\ReturnValueNotConfiguredException;

class Logincontroller extends Controller
{
    function login()
    {
        return view('login');
    }
    function checklogin(Request $req)
    {
        $this->validate($req,[
                            'email' => 'required',
                            'password' => 'required'
                             ]);

            $signup = array(
                       'email' => $req->get('email'),
                       'password' => $req->get('password'),
                       );

         if(Auth::attempt($signup))
            {
                return redirect('index')->with('success','signup successfully!');
            }
        else
            {
                return back()->with('error', 'invalid login detail');
            }
    } 

    function logout()
        {
            Auth::logout();    
            return redirect('login');
        }  
}
