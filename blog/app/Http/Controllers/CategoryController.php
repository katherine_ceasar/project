<?php
namespace App\Helpers;
namespace App\Http\Controllers;
use App\Models\category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    function search(Request $request)
    {
        // dd($request);
        if($request->ajax())
        {
            $output = '';
            $query = $request->get('query');
            $sortby = $request->get('sortby');
            $search = $request->get('search');
            if($query != '')
               {
                          $data = DB::table('categories')
                          ->orderBy($query, $sortby)
                          ->get();
               }      
             elseif($search != '')
               {
                    $data = DB::table('categories')
                    ->where('name', 'like', '%'.$search.'%')
                    ->get();
                }
            else
                {
                    $data = DB::table('categories')
                    ->get();
                }

            $total_row = $data->count();
            if($total_row > 0)
             {
                foreach($data as $row)
                {
                  $image = asset('images/'.$row->image);
                  $output .= '
                  <tr>
                    <td>'.$row->id.'</td>
                    <td>'.$row->name.'</td>
                    <td><img src="'.$image.'" height="100"  width="100" /></td>
                    <td>'.$row->no_of_order.'</td>
                    <td>'.$row->order_id.'</td>
                    <td>'.$row->status.'</td>
                    <td>'.$row->created_at.'</td>
                    <td><a href="managecategory/delete/{id}"'.$row->id.'"">Delete</a></td>
                    <td><a href="managecategory/edit/"'.$row->id.'" ">Update</a></td>
                  </tr>
                   ';
                }
             }
            else
             {
              $output = '
              <tr>
               <td align="center" colspan="10"><h3>No Data Found</h3></td>
              </tr>
              ';
             }
          $data = array(
          'table_data'  => $output,
          );
         echo json_encode($data);
        }
    }
    
//    function sort_category(Request $request)
//     {
//       $output = '';
//       $query = $request->get('query');
//       $sortby = $request->get('sortby');
//       if($request->ajax())
//         {
//           $query = $request->get('query');
//           if($query != '')
//           {
//            $data = DB::table('categories')
//            ->orderBy($query,$sortby )
//            ->get();
//           }
//             foreach($data as $row)
//             {
//                 $image = asset('images/'.$row->image);
//                 $output .= '
//                 <tr>
//                   <td>'.$row->id.'</td>
//                   <td>'.$row->name.'</td>
//                   <td><img src="'.$image.'" height="100"  width="100" /></td>
//                   <td>'.$row->no_of_order.'</td>
//                   <td>'.$row->order_id.'</td>
//                   <td>'.$row->status.'</td>
//                   <td>'.$row->created_at.'</td>
//                   <td><a href={{"managecategory/delete/"'.$row->id.'}}>Delete</a></td>
//                   <td><a href={{"managecategory/edit/"'.$row->id.']}}>Update</a></td>
//                 </tr>
//                   ';
//             }
//         }
//        $data = array(
//        'table_data'  => $output,
//        );
//       echo json_encode($data);
//     }

   function add(Request $req)
    {
        $req->validate([
            'name' => 'required',
            'image' => 'required|mimes:jpeg,jpg,gif,png',
            'no_of_order' => 'required',
            'status' => 'required'
        ]);
        
        $category = new category();
        $category->fill($req->all());
        $path = time().'.'.$req->image->extension();
        imageUpload($category->image, 'images/'.$path);
        $category->image = $path;
        $category->order_id = mt_rand( 10000, 99999);
        $category->save();
        return redirect('category')->with('success', 'Data insert successfully!');
       }

    //show category
    function show()
        {
            // $data = category::paginate(3);
             $data = category::all();
             return view('managecategory', ['categories'=>$data]);
        }


    //delete category
    function delete($id)
        {
            $data = category::find($id);
            $data->delete();
            return redirect('managecategory')->with('success', 'Data delete successfully!');
        }

    //show data in form
    function showdata($id)
        {
            $data = category::find($id);
            return view('edit', ['category'=>$data]);
        }

    //update category data
    function update(Request $req)
        {
                $data = category::find($req['id']);
                $data->name = $req->name;
                $image1= $req->image;
                if($req->hasfile('image'))
                    {
                        $path = time().'.'.$req->image->extension();
                        imageUpload($image1,'images/'.$path);
                        $data->image = $path;
                    }
                $data->no_of_order = $req->no_of_order;
                $data->order_id = $req->order_id;
                $data->status = $req->status;
                $data->save();
                return redirect('managecategory')->with('success', 'Data updated successfully!');
        }
        // function checkhelper()
        // {
        //     $a=getmytext();
        //     return $a;
        // }
        //   function list()
        // {
        //     return category::all();
        // }

        // function addapi(Request $req)
        // {
        //     return["result"=>"data insert"];
        //     // $category = new category();
            // $category->name = $req->name;

            //   $category = $req->file('image');
            // // if($req->hasfile('image'))

            //             $file = $req->file('image');
            //             $extension = $file->getClientOriginalExtension();
            //             $filename = time() .'.'.$extension;
            //             $file->move('public/images', $filename);
            //             $category->image = $filename;
            //       //  }

            // $category->no_of_order = $req->no_of_order;
            // $category->order_id = mt_rand( 10000, 99999);
            // $category->status = $req->status;
            // $category->save();
            // $result = $category->save();
            // if($result)
            // {
            //     return ["Result"=>"data added successfully"];
            // }
            // else{
            //     return ["Result"=>"fail to insert data"];
            // }

        // }



      
}
