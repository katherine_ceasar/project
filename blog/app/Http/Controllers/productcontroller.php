<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\product;
use App\Models\category;
use App\Models\Image;
use Illuminate\Support\Facades\File;
 

class ProductController extends Controller
{
  function sort_product(Request $request)
    {
      $output = '';
      $query = $request->get('query');
      $sortby = $request->get('sortby');
      if($request->ajax())
        {
          $query = $request->get('query');
          if($query != '')
            {
              $data = DB::table('products')
              ->select('products.*', 'images.image')
              ->leftJoin('images', 'products.id', '=', 'images.product_id')
              ->where('images.status', '=', 'active')
              ->orderBy($query,$sortby)
              ->get();
            }
           foreach($data as $product)
            {
              $image = asset('product_images/'.$product->image);
              $output .= '
              <tr>
                <td>'.$product->id.'</td>
                <td>'.$product->name.'</td>
                <td><img src="'.$image.'" height="100"  width="100" /></td>
                <td>'.$product->product_code.'</td>
                <td>'.$product->price.'</td>
                <td>'.$product->sale_price.'</td>
                <td>'.$product->quantity.'</td>
                <td>'.$product->created_at.'</td>
                <td><a href="manageproduct/delete/"'.$product->id.'"">Delete</a></td>
                <td><a href="manageproduct/update/"'.$product->id.'" ">Update</a></td>
              </tr>
              ';
            }
    }
    $data = array(
     'table_data'  => $output,
    );
    echo json_encode($data);
    }

    //show data in dropdown
    public function create()
    {
        $categories = Category::all(['id', 'name']);
        return view('product', compact('categories'));
    }

    function add(Request $req)
    {
        $req->validate([
                    'category_id' => 'required',
                    'name' => 'required',
                    'image' => 'required',
                    'price' => 'required',
                    'sale_price' => 'required',
                    'quantity' => 'required'
                    ]);

           // $product = new product;
            $product = new product(request()->all());
            $product->category_id = $req->category_id;
            $product->name = $req->name;
            $product->product_code = mt_rand(10000, 99999);
            $product->price = $req->price;
            $product->sale_price = $req->sale_price;
            $product->quantity = $req->quantity;
            $product->save();
            //dd($product);
           
             $lastId = $product->id;
                // dd($lastId);
                $status = "active";
                foreach ($req->image as $image) 
                 {
                   // imageUpload($image1,'images/'.$path);
                    $path = time().'.'.$image->extension();
                    imageUpload($image,'product_images/'.$path);
                    $product_image = [
                            "product_id" => $lastId,
                            "image" => $path,
                            "status" => $status,
                                    ];
                          $status = "inactive";
                    
                    //dd($product_image);
                     Image::insert($product_image);
            }
             return redirect('product')->with('success', 'Data insert successfully!');
    }

    //show product
    function show()
    {
            $data = DB::table('products')
            ->select('products.*', 'images.image')
            ->leftJoin('images', 'products.id', '=', 'images.product_id')
            ->where('images.status', '=', 'active')
            ->get();
          return view('manageproduct', ['products' => $data]);
    }

    //detele product
    function delete($id)
    {
        $data = product::find($id);
        $data->delete();

        return redirect('manageproduct')->with('success', 'Data deleted successfully!');
    }

    //show data in form
    function showdata($id)
    {
        $data = product::find($id);
        $data1 = Image::where('product_id', $id)->get();
        $categories = Category::all(['id', 'name']);
         //dd($data1);
        return view('update', ['product' => $data, 'image' => $data1, 'categories' => $categories]);
    }
//edit product form
    function edit(Request $req)
    {
        
        $data = product::find($req['id']);
        //dd($data);
        $data->name = $req->name;
        $data->category_id = $req->category_id;
        $data->product_code = $req->product_code;
        $data->price = $req->price;
        $data->sale_price = $req->sale_price;
        $data->quantity = $req->quantity;
        $data->save();

        if ($req['checkbox11'] == 0)
        {
            $status = "active";
        } 
        else 
        {
            $status = "inactive";
        }
        $s = "inactive";
        if ($req['checkbox11'] == 0) {
        $active = Image::where('product_id', $req['id'])
        ->where('status', 'active')->first();
        $active->status = $s;
        $active->save();    
        
        $inactive = Image::find($req['image_id']);
        $inactive->status = $status;
        $inactive->save();
        }
           
                $lastId = $data->id;
                 // dd($lastId);
                 $status = "inactive";
                 if($req->hasfile('image'))
                 {
                 foreach ($req->image as $image1) 
                        {
                           
                           $path = time().'.'.$image1->extension();
                           imageUpload($image1,'product_images/'.$path);
                            $product_image[] = [
                                            "product_id" => $lastId,
                                            "image" => $path,
                                            "status" => $status,
                                            ];
                       
                             //dd($product_image);
                            Image::insert($product_image);
             }
            }
        return redirect('manageproduct')->with('success', 'Data updated successfully!');
    }

//delete image from product form
    public function destroy($id)
    {
        $data = Image::find($id);
        $folder_image = 'product_images/' . $data->image;
            if (File::exists($folder_image)) {
            File::delete($folder_image);
           }
        $data->delete();
        return back()->with("success", "Image deleted successfully.");
    }
}
