<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\user;
use App\Http\Controllers\Logincontroller;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Login and signup


//Route::get('login',[user::class,'login']);
//Route::post('signup',[user::class,'addData']);  
//Route::post('login',[user::class,'check'])->name('auth.check');
//Route::get('logout',[user::class,'logout']);

Route::get('signup',[user::class,'signup']);
Route::get('login',[Logincontroller::class,'login']); 
Route::get('/',[Logincontroller::class,'login']); 
Route::post('login',[Logincontroller::class,'checklogin'])->name('auth.check');
Route::get('logout',[Logincontroller::class,'logout']);
Route::get('index',[user::class,'index']);

//category
Route::post('category',[CategoryController::class,'add'])->name('category.add');
Route::get('managecategory',[CategoryController::class,'show'])->name('category.show');
Route::get('managecategory/delete/{id}',[CategoryController::class,'delete'])->name('managecategory.delete');
Route::get('managecategory/edit/{id}',[CategoryController::class,'showdata'])->name('managecategory.showdata');
Route::post('managecategory/edit',[CategoryController::class,'update'])->name('managecategory.update');
Route::get('category',function () {
   return view('category');
});

//sorting and searching category
Route::post('managecategory/search',[CategoryController::class,'search'])->name('managecategory.search');


//PRODUCT
Route::view('product','product');
Route::get('product',[ProductController::class,'create']);
Route::post('product',[ProductController::class,'add'])->name('product.add');
Route::get('manageproduct',[ProductController::class,'show'])->name('manageproduct.show');
Route::get('manageproduct/delete/{id}',[ProductController::class,'delete'])->name('manageproduct.delete');
Route::get('manageproduct/update/{id}',[ProductController::class,'showdata'])->name('manageproduct.showdata');
Route::post('manageproduct/update',[ProductController::class,'edit'])->name('manageproduct.edit');
Route::get('deleteimage/{id}', [ProductController::class, 'destroy']);

//sorting product
Route::post('manageproduct/sort_product',[ProductController::class,'sort_product'])->name('manageproduct.sort_product');


