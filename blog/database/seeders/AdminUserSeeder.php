<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\signup;
class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        signup::create([

            'user_name' => 'komal',

            'email' => 'admin@gmail.com',

            'birthdate' =>'2000-01-01',

            'password' => bcrypt('123456'),

        ]);
    }
}
