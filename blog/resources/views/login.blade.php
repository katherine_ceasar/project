<!DOCTYPE html>
<html lang="en">
<head>
    <title>Log in</title>
      
</head>
<body>
<style>
body
{
background-color:#c5bffd;
}
.col-lg-8
{
   
    margin-top: 10%;
}

</style>
@include('flash-message')
<div class="row">
	<div class="container">
		<div class="col-lg-2">
		   </div>
        	<div class="col-lg-8">
        		<h1>Log in</h1>
         			   <hr>
                  <form action="{{route('auth.check')}}" method="POST">
                  @csrf 
                     
                     <div class="form-group">
                     <label>Email</label>
                     <input type="text" class="form-control" name="email"   id="email" >  
                     <span style="color:red">@error('email'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                     <label>Password</label>
                     <input type="password"class="form-control" name="password" id="pass" >
                     <span style="color:red">@error('password'){{$message}}@enderror</span>
                    </div>
                    
                    <button type="submit" class="btn btn-primary btn-lg btn-block" name="login">Log in</button>
                    <p>
                        Already have an account?
                       <a href="{{ asset('signup') }}" > Signup Here!</a>
                     </p>
                    </div>
                   
                   </form> 
             <div class="col-lg-2">
	     	 </div>
	   </div>
</div>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleap+is.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >
</body>
</html>
