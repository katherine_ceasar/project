<!DOCTYPE html>
<html lang="en">
<head>
<title>Edit Category</title>
</head>
<body>
@include('flash-message')
<form action={{asset('managecategory/edit/')}}  method="POST"  enctype="multipart/form-data">
 @csrf 
 <div class="row">
  <div class="container">
           <div class="col-lg-2">
            </div>
              <input type="hidden"  name="id"   value="{{$category['id']}}">  
                 <div class="col-lg-8">
                    <div class="form-group">
                     <label>Category name</label>
                     <input type="text" class="form-control" name="name"   id="cname" value="{{$category['name']}}">  
                     <span style="color:red">@error('name'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                     <label>Image</label>
                     <input type="file" class="form-control" name="image"   id="image">   
                     <img src="{{ asset('images/'.$category->image) }}" height='100'  width='100' />
                    </div>
                    
                    <div class="form-group">
                     <label>Order</label>
                     <input type="number" class="form-control" name="no_of_order"   id="order" value="{{$category['no_of_order']}}">  
                     <span style="color:red">@error('no_of_order'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                     <label>order_code</label>
                     <input type="number" class="form-control" name="order_id"   id="order" value="{{$category['order_id']}}" readonly>  
                     <span style="color:red">@error('no_of_order'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                    <label>Status</label>
                     <input type="text" class="form-control" name="status"   id="order" value="{{$category['status']}}">  
                     <span style="color:red">@error('status'){{$message}}@enderror</span>
                    </div>
                  
                    <button type="submit"  class="btn btn-primary btn-lg btn-block">Add </button>
                  </div>
                <div class="col-lg-2">
           </div>
      </div>
</div>
</form>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >
</body>
</html>