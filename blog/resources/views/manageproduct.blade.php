<!DOCTYPE html>
<html lang="en">
<head>
<title>Manage Product</title>
</head>
<body>
@include('flash-message')
<div class="bs-example">
    <ul class="nav nav-pills mb-5">
        <li class="nav-item">
            <a  href="{{ asset('index')}}"  class="nav-link " >Home</a>
        </li>
       
        <li class="nav-item dropdown">
            <a  href="{{ asset('product')}}" class="nav-link active dropdown-toggle" data-toggle="dropdown" >Product</a>
            <div class="dropdown-menu">
                <a href="{{ asset('product')}}" class="dropdown-item">Add Product</a>
                <a href="{{ asset('manageproduct')}}" class="dropdown-item">Manage Product</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a href="{{ asset('category')}}" class="nav-link  dropdown-toggle" data-toggle="dropdown">Category</a>
            <div class="dropdown-menu">
                <a href="{{ asset('category')}}" class="dropdown-item">Add Category</a>
                <a href="{{ asset('managecategory')}}" class="dropdown-item">Manage Category</a>
                
            </div>
        </li>
        
       <li class="nav-item dropdown ml-auto">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Admin</a>
            <div class="dropdown-menu dropdown-menu-right">
               <a href="{{ asset('logout')}}"class="dropdown-item">Logout</a>
            </div>
        </li>
     </ul>
</div>

<div classs="container">
	<div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
              <table class="table table-striped table-bordered"> 
						<thead>
					      <tr>
                            <th>product_id<i class="fa fa-sort-numeric-asc"  name="up"  onclick="sort_product_data('id','asc')"></i> <i class="fa fa-sort-numeric-asc"  name="down" onclick="sort_product_data('id','desc')"></i></th>
                             <th>Product Name <i class="fa fa-sort-alpha-asc"  name="up" onclick="sort_product_data('name','asc')"></i> <i class="fa fa-sort-alpha-desc"  name="down" onclick="sort_product_data('name','desc')"></i></th>
							<th>image</th>
							<th>produt_code</th>
                            <th>price</th>
                            <th>sale_price</th>
                            <th>quantity<i class="fa fa-sort-numeric-asc"  name="up" onclick="sort_product_data('quantity','asc')"></i> <i class="fa fa-sort-numeric-desc"  name="down" onclick="sort_product_data('quantity','desc')"></i></th>
                            <th>Added_date <i class="fa fa-sort-numeric-asc"  name="up" onclick="sort_product_data('created_at','asc')"></i> <i class="fa fa-sort-numeric-desc"  name="down" onclick="sort_product_data('created_at','desc')"></i></th>
				            <th> Action </th>
							<th> Action </th>
						 </tr>
				     </thead>
		     <tbody>
 @foreach($products as $product)
 <tr>
    <td>{{$product->id}}</td>
    <td>{{$product->name}}</td>
    <td><img src="{{ asset('product_images/'.$product->image) }}" height='100'  width='100' /></td>
    <td>{{$product->product_code}}</td>
    <td>{{$product->price}}</td>
    <td>{{$product->sale_price}}</td>
    <td>{{$product->quantity}}</td>
    <td>{{$product->created_at}}</td>
    <td><a href={{"manageproduct/delete/".$product->id}}>Delete</a></td>
    <td><a href={{"manageproduct/update/".$product->id}}>Update</a></td>
</tr>
@endforeach
             </tbody>
                </table>
                </div>    
            </div>
		</div>
    </div>
</div>


<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >


<script type="text/javascript">
    function sort_product_data(query,sortby)
    {
        $.ajax({
        url:"{{ route('manageproduct.sort_product') }}",
        method:'POST',
        data:{
            "_token": "{{ csrf_token() }}",
            query:query,sortby:sortby},
        dataType:'json',
        success:function(data)
        {
            $('tbody').html(data.table_data);
        }
        })
    }

</script>
</body>
</html>
