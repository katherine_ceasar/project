<!DOCTYPE html>
<html lang="en">
<head>
<title>Product</title>
</head>
<body>
@include('flash-message')
<div class="bs-example">
    <ul class="nav nav-pills mb-5">
        <li class="nav-item">
            <a  href="{{ asset('index')}}"  class="nav-link " >Home</a>
        </li>
       
        <li class="nav-item dropdown">
            <a  href="{{ asset('product')}}" class="nav-link active dropdown-toggle" data-toggle="dropdown" >Product</a>
            <div class="dropdown-menu">
                <a href="{{ asset('product')}}" class="dropdown-item">Add Product</a>
                <a href="{{ asset('manageproduct')}}" class="dropdown-item">Manage Product</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a href="{{ asset('category')}}" class="nav-link  dropdown-toggle" data-toggle="dropdown">Category</a>
            <div class="dropdown-menu">
                <a href="{{ asset('category')}}" class="dropdown-item">Add Category</a>
                <a href="{{ asset('managecategory')}}" class="dropdown-item">Manage Category</a>
                
            </div>
        </li>
        
        <li class="nav-item dropdown ml-auto">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Admin</a>
            <div class="dropdown-menu dropdown-menu-right">
               <a href="{{ asset('logout')}}"class="dropdown-item">Logout</a>
            </div>
        </li>
    </ul>
    </div>
    @if(Session::has('message'))

<p class="alert
{{ Session::get('alert-class', 'alert-info') }}">{{Session::get('message') }}</p>

@endif
<form action="product" name="Product"  role="form"  method="POST" enctype="multipart/form-data" >
@csrf 
<div class="row">
  <div class="container">
    <div class="col-lg-2">
      </div>
          <div class="col-lg-8">

                    <div class="form-group">
                        <label>Category Name</label>
                          <select name="category_id" class="form-control">
                          <option>---Select Category---</option>
                            @foreach($categories as $category)
                           <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                          </select>
                          <span style="color:red">@error('category_id'){{$message}}@enderror</span>
                    </div>
                 
                     <div class="form-group">
                     <label>Product name</label>
                     <input type="text" class="form-control" name="name"   id="pname">  
                     <span style="color:red">@error('name'){{$message}}@enderror</span>
                     </div>
                    
                     <div class="form-group">
                     <label>Image</label>
                     <input type="file" class="form-control" name="image[]"  id="image" multiple>
                     <span style="color:red">@error('image'){{$message}}@enderror</span>
                     </div>

                     <div class="form-group">
                     <label>Price</label>
                     <input type="text" class="form-control" name="price"   id="price" >
                     <span style="color:red">@error('price'){{$message}}@enderror</span>  
                     </div>

                     <div class="form-group">
                     <label>Sale Price</label>
                     <input type="text" class="form-control" name="sale_price"   id="sprice" > 
                     <span style="color:red">@error('sale_price'){{$message}}@enderror</span> 
                     </div>

                     <div class="form-group">
                     <label>Quantity</label>
                     <input type="number" class="form-control" name="quantity"   id="quantity" >  
                     <span style="color:red">@error('quantity'){{$message}}@enderror</span>
                     </div>
                    <button type="submit" name="add"  class="btn btn-primary btn-lg btn-block">Add </button>
          </div>
      <div class="col-lg-2">
    </div>
  </div>
</div>
</form>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >
</body>
</html>