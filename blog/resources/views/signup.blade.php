<!DOCTYPE html>
<html lang="en">
<head>
    <title>Signup in</title>
</head>
<body>
<style>
body
{
background-color:#c5bffd;
}
.col-lg-8
{
    margin-top: 5%;
}
</style>
@include('flash-message')

	<div class="row">
	<div class="container">
		<div class="col-lg-2">
		</div>
        	<div class="col-lg-8">
        		<h1>sign up</h1>
         			   <hr>
                      <form action="signup" method="POST">
                        @csrf 
                        <div class=results>
                        @if(session::get('success'))
                        <div class="alert alert-success">
                        {{session::get('success')}}
                        </div>
                        @endif

                        @if(session::get('fail'))
                        <div class="alert alert-danger">
                        {{session::get('fail')}}
                        </div>
                        @endif
                        </div>
                    <div class="form-group">
                     <label>User Name</label>
                     <input type="text" class="form-control" name="user_name"   id="username">  
                    <span style="color:red">@error('user_name'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                     <label>BirthDate</label>
                     <input type="date"class="form-control" name="birthdate" id="birthdate" >
                     <span style="color:red">@error('birthdate'){{$message}}@enderror</span>
                   
                    </div>
                    
				        	<div class="form-group">
                     <label>Email</label>
                     <input type="email" class="form-control" name="email"   id="email" >  
                     <span style="color:red">@error('email'){{$message}}@enderror</span>
                   </div>
                    
                   <div class="form-group">
                     <label>Password</label>
                     <input type="password"class="form-control" name="password" id="pass">
                     <span style="color:red">@error('password'){{$message}}@enderror</span>
                   
                    </div>
                    
                    <button type="submit" class="btn btn-primary btn-lg btn-block" name="signup">Log in</button>
                    <p>
                        Don't have an account?
                       <a href="{{ asset('login') }}" > Login Here!</a>
                     </p>
                    </div>

                   </form>

          <div class="col-lg-2">
		</div>
	</div>
</div>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleap+is.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >
</body>
</html>
