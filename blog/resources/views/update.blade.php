<!DOCTYPE html>
<html lang="en">
<head>
<title>Edit Product</title>
</head>
<body>
@include('flash-message')
<form action="{{asset('manageproduct/update/')}}"  method="POST" enctype="multipart/form-data">
 @csrf 
 <div class="row">
  <div class="container">
           <div class="col-lg-2">
            </div>
              <input type="hidden"  name="id"   value="{{$product['id']}}">  
                 <div class="col-lg-8">

                   <div class="form-group">
                        <label>Category Name</label>
                          <select name="category_id" class="form-control">
                            <option>---Select Category---</option>
                             @foreach($categories as $category)
                             <option value="{{$category['id'] }}{{old('id')}}" selected>{{$category['name'] }} {{old('$category->name')}}</option>
                             @endforeach
                           </select>
                          <span style="color:red">@error('category_id'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                     <label>Product name</label>
                     <input type="text" class="form-control" name="name"   id="cname" value="{{$product['name']}}">  
                     <span style="color:red">@error('name'){{$message}}@enderror</span>
                    </div>

                     <div class="form-group">
                     <label>Product Code</label>
                     <input type="text" class="form-control" name="product_code"   id="pname" value="{{$product['product_code']}}" readonly>  
                     </div>


                     <div class="form-group">
                     <label>Image</label>
                     <input type="file" class="form-control" name="image[]"  id="image" multiple>
                     </div>
                   
                    @foreach($image as $key => $value)
                    @if ($value->status=='active')
                        <img class="active-image m-2 border border-success" src="{{ asset('product_images/'.$value->image) }}" height='100'  width='100' />
                        <input type="checkbox" name="checkbox11"  value="1"   @if ($value->status=='active') checked @endif>
                        <label for="test" style="padding-left: 15px!important;">active</label>  
                        <a href="{{url('deleteimage/'.$value->id)}}"  class="btn btn-sm btn-danger ml-2">Delete</button></a>       
                    @else
                       <img src="{{ asset('product_images/'.$value->image) }}" height='100'  width='100' />
                       <input type="checkbox" name="checkbox11" value="0"   @if ($value->status=='active') checked @endif>
                       <label for="test" style="padding-left: 15px!important;">inactive</label>
                    @if ($value->status=='inactive')
                     <input type="hidden"  name="image_id"   value="{{$value->id}}">  
                    @endif
                     <a href="{{url('deleteimage/'.$value->id)}}"  class="btn btn-sm btn-danger ml-2">Delete</button></a>
                    @endif
                    @endforeach
                   
                     <div class="form-group">
                     <label>Price</label>
                     <input type="text" class="form-control" name="price"   id="price" value="{{$product['price']}}">  
                     <span style="color:red">@error('price'){{$message}}@enderror</span>
                    </div>

                
                    <div class="form-group">
                    <label>Sale_price</label>
                     <input type="text" class="form-control" name="sale_price"   id="sale_price" value="{{$product['sale_price']}}">  
                     <span style="color:red">@error('sale_price'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                    <label>Quantity</label>
                     <input type="text" class="form-control" name="quantity"   id="quantity" value="{{$product['quantity']}}">  
                     <span style="color:red">@error('quantity'){{$message}}@enderror</span>
                    </div>
                  
                    <button type="submit"  class="btn btn-primary btn-lg btn-block">Update </button>
                  </div>
                <div class="col-lg-2">
           </div>
      </div>
</div>
</form>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >
</body>
</html>