<!DOCTYPE html>
<html lang="en">
<head>
<title>Home</title>
</head>
<body>

<div class="bs-example">
    <ul class="nav nav-pills mb-5">
        <li class="nav-item">
            <a  href="{{ asset('index')}}" class="nav-link active">Home</a>
        </li>
       
        <li class="nav-item dropdown">
            <a  href="{{ asset('product')}}" class="nav-link dropdown-toggle" data-toggle="dropdown">Product</a>
            <div class="dropdown-menu">
                <a href="{{ asset('product')}}" class="dropdown-item">Add Product</a>
                <a href="{{ asset('manageproduct')}}" class="dropdown-item">Manage Product</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a href="{{ asset('category')}}" class="nav-link dropdown-toggle" data-toggle="dropdown">Category</a>
            <div class="dropdown-menu">
                <a href="{{ asset('category')}}" class="dropdown-item">Add Category</a>
                <a href="{{ asset('managecategory')}}" class="dropdown-item">Manage Category</a>
                
            </div>
        </li>

        <li class="nav-item dropdown ml-auto">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Admin</a>
            <div class="dropdown-menu dropdown-menu-right">
               <a href="{{ asset('logout')}}"class="dropdown-item">Logout</a>
            </div>
        </li>
    </ul>
</div>
                   
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >
</body>
</html>