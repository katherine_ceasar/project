<!DOCTYPE html>
<html lang="en">
<head>
<title>Home</title>
</head>
<body>
<div class="bs-example">
    <ul class="nav nav-pills mb-5">
        <li class="nav-item">
            <a  href="index.php"  class="nav-link active " >Home</a>
        </li>
       
        <li class="nav-item dropdown">
            <a  href="product.php" class="nav-link dropdown-toggle" data-toggle="dropdown" >Product</a>
            <div class="dropdown-menu">
                <a href="product.php" class="dropdown-item">Add Product</a>
                <a href="manageproduct.php" class="dropdown-item">Manage Product</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a href="category" class="nav-link  dropdown-toggle" data-toggle="dropdown">Category</a>
            <div class="dropdown-menu">
                <a href="category.php" class="dropdown-item">Add Category</a>
                <a href="managecategory.php" class="dropdown-item">Manage Category</a>
                
            </div>
        </li>
        
        <li class="nav-item dropdown ml-auto">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Admin</a>
            <div class="dropdown-menu dropdown-menu-right">
               <a href="logout.php"class="dropdown-item">Logout</a>
            </div>
        </li>
    </ul>
</div>
<div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="activ  e"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <!-- The slideshow -->
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <a href="login.php">
                    <img src="img/1.jpg">
                  </a>
                </div>
                <div class="carousel-item">
                  <a href="login.php">
                    <img src="img/2.jpg">
                  </a>
                </div>
                <div class="carousel-item">
                  <a href="login.php">
                   <img src="img/3.jpg">
                  </a>
                </div>
              </div>
  
          <!-- Left and right controls -->
          <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </a>
          <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
          </a>
</div>
<div class="mini-footer">
    <div class="container">
        <div class="row">
           <div class="col-md-12">
              <div class="copyright-text">
                <p>© 2021
                 <a href="#">DigiPro</a>. All rights reserved. Created by
                 <a href="#">AazzTech</a>
                </p>
              </div>
            <div>
        </div>
    <div>
</div>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/style.css">
</body>
</html>