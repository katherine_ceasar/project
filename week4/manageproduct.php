<?php
session_start();
include("conn.php");
if(isset($_POST['update']))
{
   if(($_POST['name'] == "") || ($_FILES['image'] == "") || ($_POST['price'] == "") || ($_POST['sale_price'] == "") || ($_POST['quantity'] == ""))
    {
      echo '<script>alert("First click on edit button")</script>';
    }
    
    {
        $name=$_POST['name'];
        $image = $_FILES['image']['name'];
        $tempname=$_FILES['image']['tmp_name'];
        $filePath="img/.jpeg";
        $image_Path = "img/".$image;
        $price=$_POST['price'];
        $sale_price=$_POST['sale_price'];
        $quantity=$_POST['quantity'];
        move_uploaded_file( $tempname, $image_Path);
        $sql="UPDATE `product` SET `name`='$name',`image`='$image',`price`='$price',`sale_price`='$sale_price',`quantity`='$quantity',`updated_date`=now() WHERE  product_id={$_REQUEST['product_id']}";

               if(mysqli_query($conn,$sql))
                {
                        echo '<script>confrim("updated successfully")</script>';
                }
                else
                {
                        echo '<script>confrim("updated successfully")</script>';
                }
         }
}
if(isset($_REQUEST['delete']))
{
   
    $sql="DELETE FROM `product` WHERE product_id={$_REQUEST['product_id']}";
    if(mysqli_query($conn,$sql))
    {
        echo '<script>confrim("Deleted successfully")</script>';
    }
    else
    {
        echo '<script>alert("Not deleted ")</script>';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Manage Product</title>
</head>
<body>
    <div class="bs-example">
        <ul class="nav nav-pills mb-5">
                <li class="nav-item">
                    <a  href="index.php"  class="nav-link">Home</a>
                </li>
             
                <li class="nav-item dropdown">
                    <a  href="product.php" class="nav-link active dropdown-toggle" data-toggle="dropdown" >Product</a>
                        <div class="dropdown-menu">
                                <a href="product.php" class="dropdown-item">Add Product</a>
                                <a href="manageproduct.php" class="dropdown-item">Manage Product</a>
                        </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="category" class="nav-link  dropdown-toggle" data-toggle="dropdown">Category</a>
                        <div class="dropdown-menu">
                                <a href="category.php" class="dropdown-item">Add Category</a>
                                <a href="managecategory.php" class="dropdown-item">Manage Category</a>
                                
                        </div>
                </li>
                
                <li class="nav-item dropdown ml-auto">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Admin</a>
                        <div class="dropdown-menu dropdown-menu-right">
                             <a href="logout.php"class="dropdown-item">Logout</a>
                        </div>
                </li>
        </ul>
</div>

<div classs="container">
 <div class="row">
    <div class="col-lg-12">
      <div class="col-lg-6">
        <div class="table-responsive">
                 <table class="table">
                    <thead>
                        <tr>
                            <th>Product_id</th>
                            <th>Category_id</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Product_Code </th>
                            <th> Price </th>
                            <th> Sale_Price </th>
                            <th> Quantity </th>
                            <th> Added_date </th>
                            <th> Action </th>
                            <th> Action </th>
                        </tr>
                    </thead>
             <tbody>
         </div>
<?php
    include "conn.php";
    $records = mysqli_query($conn,"SELECT * FROM `product`"); // fetch data from database
        while($data = mysqli_fetch_array($records))
            {
?>
    <tr>
            <td><?php echo $data['product_id']; ?></td>
            <td><?php echo $data['category_id']; ?></td>
            <td><?php echo $data['name']; ?></td>
            <td><?php echo "<img src='img/".$data['image']."'  style='max-height:100%; max-width:100%'>";  ?> </td>
            <td><?php echo $data['product_code']; ?></td>
            <td><?php echo $data['price']; ?></td>
            <td><?php echo $data['sale_price']; ?></td>
            <td><?php echo $data['quantity']; ?></td>
            <td><?php echo $data['added_date']; ?></td>
            <?php echo '<td><form action="" method="POST"> <input type="hidden" 
                    name="product_id" value='.$data['product_id'].'><input type="submit" 
                    class="btn btn-sm btn-danger" name="delete" 
                    value="Delete"></form></td>'?>

            <?php echo '<td><form action="" method="POST"> <input type="hidden" 
                    name="product_id" value='.$data['product_id'].'><input type="submit" 
                    class="btn btn-sm btn-warning" name="edit" 
                    value="Edit"></form></td>'?>
    </tr> 
<?php
}
?>
</tbody>
</table>
            </div>
        </div>
                <div class="col-lg-6" >
                            <?php
                                if(isset($_REQUEST['edit']))
                                    {
                                        $sql="SELECT * FROM `product` WHERE `product_id`={$_REQUEST['product_id']}";
                                        $result=mysqli_query($conn,$sql);
                                        $row=mysqli_fetch_assoc($result);
                                    }
                            ?>
                        <form action="" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Product name</label>
                                <input type="text" class="form-control" name="name" value="<?php if(isset($row["name"])){echo $row["name"];}?>"> 
                            </div>

                            <div class="form-group">
                                <label>Image</label>
                                <input type="file" class="form-control" name="image"  id="image" >    
                            </div>

                            <div class="form-group">
                                <label>Price</label>
                                <input type="text" class="form-control" name="price" value="<?php if(isset($row["price"])){echo $row["price"];}?>"> 
                            </div>

                            <div class="form-group">
                                <label>Price</label>
                                <input type="text" class="form-control" name="sale_price" value="<?php if(isset($row["sale_price"])){echo $row["sale_price"];}?>"> 
                            </div>

                            <div class="form-group">
                                <label>quantity</label>
                                <input type="text" class="form-control" name="quantity" value="<?php if(isset($row["quantity"])){echo $row["quantity"];}?>"> 
                            </div>
                                        
                            <input type="hidden" name="product_id" value="<?php echo $row['product_id'];?>">
                            <button type="submit" class="btn btn-success" name="update">update</button>
                        </form>
                </div>
        </div>
    </div>
   </div> 
</div>

<meta charset="utf-8">  
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
</body>
</html>
