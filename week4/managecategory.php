<?php
session_start();
include("conn.php");
if(isset($_POST['update']))
 {
    if(($_POST['name'] == "") || ($_FILES['category_image'] == "") || ($_POST['no_of_order'] == "") || ($_POST['status'] == ""))
    {
      echo '<script>alert("First click on edit button")</script>';
    }
	{
		$name=$_POST['name'];
		$category_image = $_FILES['category_image']['name'];
        $tempname=$_FILES['category_image']['tmp_name'];
        $filePath="img/.jpeg";
        $image_Path = "img/".$category_image;
		$no_of_order=$_POST['no_of_order'];
		$status=$_POST['status'];
		move_uploaded_file($tempname, $image_Path);
		$sql="UPDATE `category` SET `name`='$name',`category_image`='$category_image',`no_of_order`='$no_of_order',`status`='$status',`updated_date`=now() WHERE category_id={$_REQUEST['category_id']}";
		 if(mysqli_query($conn,$sql))
			{
			 	echo '<script>confrim("updated successfully")</script>';
			}
			else
			{
		    	echo '<script>confrim("updated successfully")</script>';
			}
	}
 }
if(isset($_REQUEST['delete']))
{
	$sql="DELETE FROM `category` WHERE category_id={$_REQUEST['category_id']}";
	if(mysqli_query($conn,$sql))
	{
		echo '<script>confrim("Deleted successfully")</script>';
	}
	else
	{
		echo '<script>alert("Not deleted ")</script>';
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Manage Category</title>
</head>
<body>
	<div class="bs-example">
		<ul class="nav nav-pills mb-5">
				<li class="nav-item">
						<a  href="index.php"  class="nav-link">Home</a>
				</li>
			 
				<li class="nav-item dropdown">
						<a  href="product.php" class="nav-link dropdown-toggle" data-toggle="dropdown" >Product</a>
						<div class="dropdown-menu">
								<a href="product.php" class="dropdown-item">Add Product</a>
								<a href="manageproduct.php" class="dropdown-item">Manage Product</a>
						</div>
				</li>
				<li class="nav-item dropdown">
						<a href="category" class="nav-link active  dropdown-toggle" data-toggle="dropdown">Category</a>
						<div class="dropdown-menu">
								<a href="category.php" class="dropdown-item">Add Category</a>
								<a href="managecategory.php" class="dropdown-item">Manage Category</a>
						</div>
				</li>
				
				<li class="nav-item dropdown ml-auto">
						<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Admin</a>
						<div class="dropdown-menu dropdown-menu-right">
							 <a href="logout.php"class="dropdown-item">Logout</a>
						</div>
				</li>
		</ul>
</div>

<div classs="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="col-lg-6">
		 		<div class="table-responsive">
		 			 <table class="table">
						<thead>
					      <tr>
							<th>Category_id</th>
							<th>Name</th>
							<th>Images</th>
							<th> No_of_order </th>
							<th> Order_id </th>
							<th> Status </th>
							<th> Added_date </th>
							<th> Action </th>
							<th> Action </th>
						 </tr>
				     </thead>
		     <tbody>
<?php
include "conn.php";
$records = mysqli_query($conn,"SELECT * FROM `category`"); // fetch data from database
while($data = mysqli_fetch_array($records))
{
?>
	     <tr>
			<td><?php echo $data['category_id']; ?></td>
			<td><?php echo $data['name']; ?></td>
			<td><?php	echo "<img src='img/".$data['category_image']."' style='max-height:100%; max-width:100%'>";  ?> </td>
            <td><?php echo $data['no_of_order']; ?></td>
			<td><?php echo $data['order_id']; ?></td>
			<td><?php echo $data['status']; ?></td>
			<td><?php echo $data['added_date']; ?></td>
				<?php echo '<td><form action="" method="POST"> <input type="hidden" 
					name="category_id" value='.$data['category_id'].'><input type="submit" 
					class="btn btn-sm btn-danger" name="delete" 
					value="Delete"></form></td>'?>
					<?php echo '<td><form action="" method="POST"> <input type="hidden" 
					name="category_id" value='.$data['category_id'].'><input type="submit" 
					class="btn btn-sm btn-warning" name="edit" 
					value="Edit"></form></td>'?>

</tr> 
<?php
}
?>
			</tbody>
		</table>
  	</div>
</div>
			<div class="col-lg-6">
				<?php
					if(isset($_REQUEST['edit']))
						{
							$sql="SELECT * FROM `category` WHERE category_id={$_REQUEST['category_id']}";
							$result=mysqli_query($conn,$sql);
							$row=mysqli_fetch_assoc($result);
						}
				?>
				<form action="" method="POST" enctype="multipart/form-data">
					<div class="form-group">
    					<label>Category name</label>
						<input type="text" class="form-control" name="name" value="<?php if(isset($row["name"])){echo $row["name"];}?>"> 
					</div>

					<div class="form-group">
						<label>Image</label>
						<input type="file" class="form-control" name="category_image"  id="image">    
					</div>

					<div class="form-group">
						<label>Order</label>
						<input type="number" class="form-control" name="no_of_order"   id="order" value="<?php if(isset($row["no_of_order"])){echo $row["no_of_order"];}?>">   
					</div>

					<div class="form-group">
						<label>Status</label>
						<input type="text" class="form-control" name="status"   id="status" value="<?php if(isset($row["status"])){echo $row["status"];}?>">   
					</div>

					<input type="hidden" name="category_id" value="<?php echo $row['category_id'];?>">
						<button type="submit" class="btn btn-success" name="update">update</button>
				</form>
			</div>
		</div>
	</div>	 
  </div>
 <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
</body>
</html>
