<?php
session_start();
include("conn.php");
function generate_thumb_now($field_name = '',$target_folder ='',$file_name = '', $thumb = FALSE, $thumb_folder = '', $thumb_width = '',$thumb_height = ''){
         //folder path setup
         $target_path = $target_folder;
         $thumb_path = $thumb_folder;   
         //file name setup
    $filename_err = explode(".",$_FILES[$field_name]['name']);
    $filename_err_count = count($filename_err);
    $file_ext = $filename_err[$filename_err_count-1];
     if($file_name != '')
     {
        $fileName = $file_name.'.'.$file_ext;
      }
    else
    {
        $fileName = $_FILES[$field_name]['name'];
    }   
    //upload image path
    $upload_image = $target_path.basename($fileName);   
    //upload image
    if(move_uploaded_file($_FILES[$field_name]['tmp_name'],$upload_image))
    {
         //thumbnail creation
        if($thumb == TRUE)
        {
            $thumbnail = $thumb_path.$fileName;
            list($width,$height) = getimagesize($upload_image);
            $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
            switch($file_ext){
                case 'jpg':
                    $source = imagecreatefromjpeg($upload_image);
                    break;
                case 'jpeg':
                    $source = imagecreatefromjpeg($upload_image);
                    
                default:
                    $source = imagecreatefromjpeg($upload_image);
            }
       imagecopyresized($thumb_create, $source, 0, 0, 0, 0, $thumb_width, $thumb_height, $width,$height);
            switch($file_ext){
                case 'jpg' || 'jpeg':
                    imagejpeg($thumb_create,$thumbnail,100);
                    break;
               default:
                    imagejpeg($thumb_create,$thumbnail,100);
            }
        }
        return $fileName;
     }
    else
    {
        return false;
     }
    }
    if(!empty($_FILES['image']['name'])){       
    $upload_img = generate_thumb_now('image','img/','',TRUE,'img /thumb/','400','320');

    //full path of the thumbnail image
    $thumb_src = 'img/thumb/'.$upload_img;

    }else{

    //if form is not submitted, below variable should be blank
    $thumb_src = '';
    $message = '';
    }
    
 if(isset($_POST['add']))
  {
   if(($_POST['product_name'] == "") || ($_FILES['image'] == "") || ($_POST['price'] == "") || ($_POST['sales_price'] == "")  || ($_POST['quantity'] == ""))
    {
      echo '<script>alert("All Fields are required")</script>';
    }
    else
      {
       $name = $_POST['name'];
       $product_name=$_POST['product_name'];

      $image = $_FILES['image']['name'];
      $tempname=$_FILES['image']['tmp_name'];
      $filePath="img/.jpeg";
      $image_Path = "img/".$image;
      move_uploaded_file($tempname, $image_Path);

       $product_code = (rand(1,1000));
       $price=$_POST['price'];
       $sales_price=$_POST['sales_price'];
       $quantity=$_POST['quantity'];
       $sql="INSERT INTO `product`(`category_id`, `name`, `image`, `product_code`, `price`, `sale_price`, `quantity`, `added_date`) VALUES ('$name','$product_name','$image','$product_code','$price','$sales_price','$quantity',now())";
       $sql1="insert into image (product_id, image) select product_id, image from product";
      mysqli_query($conn,$sql1);
      if(mysqli_query($conn,$sql))  
        {
          echo '<script>confirm("Data insert successfully")</script>';
        }
       else
        {
          echo "not insert";
        }
      } 
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Product</title>
</head>
<body>
<div class="bs-example">
    <ul class="nav nav-pills mb-5">
        <li class="nav-item">
            <a  href="index.php"  class="nav-link " >Home</a>
        </li>
       
        <li class="nav-item dropdown">
            <a  href="product.php" class="nav-link active dropdown-toggle" data-toggle="dropdown" >Product</a>
            <div class="dropdown-menu">
                <a href="product.php" class="dropdown-item">Add Product</a>
                <a href="manageproduct.php" class="dropdown-item">Manage Product</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a href="category" class="nav-link  dropdown-toggle" data-toggle="dropdown">Category</a>
            <div class="dropdown-menu">
                <a href="category.php" class="dropdown-item">Add Category</a>
                <a href="managecategory.php" class="dropdown-item">Manage Category</a>
            </div>
        </li>
        
        <li class="nav-item dropdown ml-auto">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Admin</a>
            <div class="dropdown-menu dropdown-menu-right">
               <a href="logout.php"class="dropdown-item">Logout</a>
            </div>
        </li>
    </ul>
</div>
<form name="Product"  role="form"  method="POST" enctype="multipart/form-data"  onsubmit = "return(validate());" >
<div class="row">
  <div class="container">
    <div class="col-lg-2">
      </div>
          <div class="col-lg-8">
             <div class="form-group">
                 <label>Category</label>
                      <?php
                        $sql = "SELECT category_id, name FROM category";
                        $result = $conn->query($sql);
                        echo "<select name='name'   class='form-control'>";
                          if ($result->num_rows > 0)
                            {
                              // output data of each row
                              while($row = $result->fetch_assoc())
                                 {
                                    echo "<option value='" . $row['category_id'] ."'>" . $row['name'] ."</option>";
                                  }
                                    echo "</select>";
                            } 
                          else
                            {
                              echo "0 results";
                            }
                      ?>
              </div>
                     <div class="form-group">
                     <label>Product  name</label>
                     <input type="text" class="form-control" name="product_name"   id="pname">  
                     </div>

                     <div class="form-group">
                     <label>Image</label>
                     <input type="file" class="form-control" name="image"  id="image">
                     </div>

                     <div class="form-group">
                     <label>Price</label>
                     <input type="text" class="form-control" name="price"   id="price" >  
                     </div>

                     <div class="form-group">
                     <label>Sale Price</label>
                     <input type="text" class="form-control" name="sales_price"   id="sprice" >  
                     </div>

                     <div class="form-group">
                     <label>Quantity</label>
                     <input type="number" class="form-control" name="quantity"   id="quantity" >  
                     </div>
                    <button type="submit" name="add"  class="btn btn-primary btn-lg btn-block">Add </button>
          </div>
      <div class="col-lg-2">
    </div>
  </div>
</div>
</form>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<script>
 function validate() 
      {
        if(document.Product.product_name.value == "" ) 
         {
            alert( "Please enter productName" );
            return false;
         }
         if(document.Product.image.value == "" ) 
         {
            alert( "Please enter image" );
            return false;
         }
         if(document.Product.price.value == "" ) 
         {
            alert( "Please enter price" );
            return false;
         }
         if(document.Product.sales_price.value == "" ) 
         {
            alert( "Please enter sales price" );
            return false;
         }
         if(document.Product.quantity.value == "" ) 
         {
            alert( "Please enter quantity" );
            return false;
         }
         return( true );
      }
  
</script>
</body>
</html>
