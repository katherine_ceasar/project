<?php
session_start();
include("conn.php");

function generate_thumb_now($field_name = '',$target_folder ='',$file_name = '', $thumb = FALSE, $thumb_folder = '', $thumb_width = '',$thumb_height = ''){
         //folder path setup
         $target_path = $target_folder;
         $thumb_path = $thumb_folder;   
         //file name setup
    $filename_err = explode(".",$_FILES[$field_name]['name']);
    $filename_err_count = count($filename_err);
    $file_ext = $filename_err[$filename_err_count-1];
     if($file_name != '')
     {
        $fileName = $file_name.'.'.$file_ext;
      }
    else
    {
        $fileName = $_FILES[$field_name]['name'];
    }   
    //upload image path
    $upload_image = $target_path.basename($fileName);   
    //upload image
    if(move_uploaded_file($_FILES[$field_name]['tmp_name'],$upload_image))
    {
         //thumbnail creation
        if($thumb == TRUE)
        {
            $thumbnail = $thumb_path.$fileName;
            list($width,$height) = getimagesize($upload_image);
            $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
            switch($file_ext){
                case 'jpg':
                    $source = imagecreatefromjpeg($upload_image);
                    break;
                case 'jpeg':
                    $source = imagecreatefromjpeg($upload_image);
                    break;
                case 'png':
                    $source = imagecreatefrompng($upload_image);
                    break;
                case 'gif':
                    $source = imagecreatefromgif($upload_image);
                     break;
                default:
                    $source = imagecreatefromjpeg($upload_image);
            }
       imagecopyresized($thumb_create, $source, 0, 0, 0, 0, $thumb_width, $thumb_height, $width,$height);
            switch($file_ext){
                case 'jpg' || 'jpeg':
                    imagejpeg($thumb_create,$thumbnail,100);
                    break;
                case 'png':
                    imagepng($thumb_create,$thumbnail,100);
                    break;
                case 'gif':
                    imagegif($thumb_create,$thumbnail,100);
                     break;
                default:
                    imagejpeg($thumb_create,$thumbnail,100);
            }
        }
        return $fileName;
     }
    else
    {
        return false;
     }
    }
    if(!empty($_FILES['image']['name'])){       
    $upload_img = generate_thumb_now('image','img/','',TRUE,'img /thumb/','400','320');

    //full path of the thumbnail image
    $thumb_src = 'img/thumb/'.$upload_img;

    //set success and error messages
    $message = $upload_img?"<span style='color:#008000;'>Image thumbnail created successfully.</span>":"<span style='color:#F00000;'>Some error occurred, please try again.</span>";

    }else{

    //if form is not submitted, below variable should be blank
    $thumb_src = '';
    $message = '';
    }
    
if(isset($_POST['add']))
  {
   if(($_POST['name'] == "") || ($_FILES['image'] == "") || ($_POST['order'] == "") || ($_POST['status'] == ""))
    {
      echo '<script>alert("All Fields are required")</script>';
    }
    else
    {
      $name=$_POST['name'];

      $image = $_FILES['image']['name'];
      $tempname=$_FILES['image']['tmp_name'];
      $filePath="img/.jpeg";
      $image_Path = "img/".$image;

      $order=$_POST['order'];
      $status=$_POST['status'];
      $order_id = (rand(1,10000));
      move_uploaded_file($tempname, $image_Path);
      $sql="INSERT INTO `category`(`name`, `category_image`, `no_of_order`,`order_id`,`status`,`added_date`) VALUES ('$name','$image','$order','$order_id','$status',now())";


      if(mysqli_query($conn,$sql))
        {
          echo '<script>confirm("Data insert successfully")</script>';
        }
       else
        {
          echo "not insert";
        }
      } 
   }

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Category</title>
</head>
<body>
<div class="bs-example">
    <ul class="nav nav-pills mb-5">
        <li class="nav-item">
            <a  href="index.php"  class="nav-link " >Home</a>
        </li>
       
        <li class="nav-item dropdown">
            <a  href="product.php" class="nav-link dropdown-toggle" data-toggle="dropdown" >Product</a>
            <div class="dropdown-menu">
                <a href="product.php" class="dropdown-item">Add Product</a>
                <a href="manageproduct.php" class="dropdown-item">Manage Product</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a href="category" class="nav-link active  dropdown-toggle" data-toggle="dropdown">Category</a>
            <div class="dropdown-menu">
                <a href="category.php" class="dropdown-item">Add Category</a>
                <a href="managecategory.php" class="dropdown-item">Manage Category</a>
                
            </div>
        </li>
        
        <li class="nav-item dropdown ml-auto">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Admin</a>
            <div class="dropdown-menu dropdown-menu-right">
               <a href="logout.php"class="dropdown-item">Logout</a>
            </div>
        </li>
    </ul>
</div>
<form name="category"  role="form"  method="POST" enctype="multipart/form-data"  onsubmit = "return(validate());" >
 <div class="row">
  <div class="container">
           <div class="col-lg-2">
            </div>
               <div class="col-lg-8">
                    <div class="form-group">
                     <label>Category name</label>
                     <input type="text" class="form-control" name="name"   id="cname" value="<?php if(isset($data["name"])){echo $data["name"];}?>">  
                    </div>

                    <div class="form-group">
                     <label>Image</label>
                     <input type="file" class="form-control" name="image"  id="image">  
                    </div>

                    <div class="form-group">
                     <label>Order</label>
                     <input type="number" class="form-control" name="order"   id="order" >  
                    </div>

                    <div class="form-group">
                     <label>Status</label>
                          <select id="status" name="status" class="form-control">
                            <option disabled selected>-- Select Status --</option>
                                <option value="active">active</option>
                                <option value="inactive">inactive</option>
                          </select>
                    </div>
                  
                    <button type="submit" name="add"  class="btn btn-primary btn-lg btn-block" value="Upload">Add </button>
                  </div>
                <div class="col-lg-2">
           </div>
      </div>
</div>
</form>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">

<script>
 function validate() 
      {
        if(document.category.name.value == "" ) 
         {
            alert( "Please enter categoryName" );
            return false;
         }
         if(document.category.image.value == "" ) 
         {
            alert( "Please enter image" );
            return false;
         }
         if(document.category.order.value == "" ) 
         {
            alert( "Please enter number of order" );
            return false;
         }
          return( true );
      }
  
</script>
</body>
</html>
